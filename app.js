const express = require("express");

const app = express();

// require("./test");

const totalSupply = require("./src/routes/totalSupply");

app.use("/coin", totalSupply);

module.exports = app;
