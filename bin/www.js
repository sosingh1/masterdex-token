require("dotenv").config();
const app = require("../app");
const http = require("http");

const port = process.env.PORT || 3000;
const host = process.env.HOST || `127.0.0.1`;

let server = http.createServer(app);

server.listen(port, host);

server.on("listening", () => {
  console.log(`server is running on ${port} at ${host}`);
});
