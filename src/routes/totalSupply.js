const express = require("express");
const router = express.Router();

const { getTotalSupply } = require("../controllers/totalSupply");
router.get("/total-supply", getTotalSupply);

module.exports = router;
