const { Web3 } = require("web3");
const { config } = require("../../config/config");
const { ERC20ABI } = require("../../config/abis");
let web3 = new Web3(config.web3Provider.http);

const getTotalSupply = async (req, res) => {
  try {
    let contractInstance = new web3.eth.Contract(
      ERC20ABI,
      "0xf0610eb7d8EE12D59412DA32625d5e273E78FF0b"
    );
    let totalSupply =
      parseInt(await contractInstance.methods.totalSupply().call()) / 10 ** 18;
    return res.status(200).json(totalSupply);
  } catch (e) {
    console.log("getTotalSupply", e);
    return res.status(400).json({
      message: "Something Went Wrong",
      success: false,
    });
  }
};

module.exports = { getTotalSupply };
